package example.hello;

import java.rmi.RemoteException;
import org.apache.log4j.Logger;
import example.interfaces.hello.HelloIF;

public class HelloImpl implements HelloIF {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(HelloImpl.class);

	public HelloImpl() {
		logger.debug("Constructor");
		logger.debug("Done");
	}

	public String sayHello() throws RemoteException {
		logger.debug("Returning message to client");
        return "Renee Vroedsteijn - Wesley Heesters";
    }

}
